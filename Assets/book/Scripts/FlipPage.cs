﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class FlipPage : MonoBehaviour
{

   

    private bool isClicked;

    private DateTime startTime;
    private DateTime endTime;

    private Vector3 rotationVector;
    private Vector3 startPosition;
    private Quaternion startRotation;

    public GameObject[] paginas;
    private int currentPage;
    

    private void Start()
    {
        startRotation = transform.rotation;
        startPosition = transform.position;

        currentPage =0;

        for (int i = 1; i < paginas.Length; i++)
        {
            paginas[i].SetActive(false);
        }

    }


    private void Update()
    {
       
      if (isClicked)
      {           
            transform.Rotate(rotationVector * Time.deltaTime);

            endTime = DateTime.Now;

         if ((endTime - startTime).TotalSeconds >= 0.7)
          {
              isClicked = false;
              transform.position = startPosition;
              transform.rotation = startRotation;  

          }
            
        }            
    }
  

  
    public void TurnNextPage()
    {
        isClicked = true;
        startTime = DateTime.Now;
        rotationVector = new Vector3(0, 180, 0);

        currentPage++;

        paginas[currentPage].SetActive(true);
        paginas[currentPage-1].SetActive(false);



    }

    public void TurnPreviousPage()
    {
        isClicked = true;
        startTime = DateTime.Now;

        Vector3 newRotation = new Vector3(startRotation.x, 180, startRotation.z);
        transform.rotation = Quaternion.Euler(newRotation);

        rotationVector = new Vector3(0, -180, 0);

        
        currentPage--;
        paginas[currentPage].SetActive(true);
        paginas[currentPage + 1].SetActive(false);




    }

  
}
