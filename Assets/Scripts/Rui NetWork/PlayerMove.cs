﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerMove : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField] float speed;
    public float jumpForce;
    bool isGrounded;

    public float gravity = -9.81f;
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    PhotonView PV;

    private bool isWalk;

    private FMOD.Studio.EventInstance walk;
    public float tempoPassos = 0.25f;

   

    private void Awake()
    {
        
        rb = GetComponent<Rigidbody>();
        PV = GetComponent<PhotonView>();

        //walk = FMODUnity.RuntimeManager.CreateInstance("event:/Player1/FootSteps");
    }
    void Start()
    {
        if (!PV.IsMine)
        {
            Destroy(GetComponentInChildren<Camera>().gameObject);
            Destroy(rb);
        }
        //InvokeRepeating(nameof(PlayerSoundWalk), 0, tempoPassos);
        StartCoroutine(nameof(PlayerSoundWalk));
    }
    
    private bool CheckGrounded()
    {
        return isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
    }
    
    private void Update()
    {
        if (!PV.IsMine)
            return;

        Move();
    }

    private void Move()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");


        isWalk = Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0; 
        
        Vector3 movement = new Vector3(horizontal, 0, vertical).normalized * speed * Time.deltaTime; //quanto quero mover 

        Vector3 newPosition = rb.position + rb.transform.TransformDirection(movement); //calcula a nova posicao

        rb.MovePosition(newPosition); //assigna a nova posicao

    }

    IEnumerator PlayerSoundWalk()
    {
        while (true)
        {
            if (isWalk)
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/Player1/FootSteps");
                yield return new WaitForSeconds(tempoPassos);
            }
            else
            {
                yield return null; 
            }
        }
    }
    
}
