﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;
using System.Linq;
using Photon.Realtime;

public class PlayerManager : MonoBehaviour
{
    PhotonView PV;

    public GameObject spawnPoint1;
    public GameObject spawnPoint2;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
       
    }

    void Start()
    {
        if (PV.IsMine)
        {
            CreatedController();      
        }
    }

    void CreatedController()
    { 
        if(PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Player"), spawnPoint1.transform.position, Quaternion.identity);          
        }
        else
        {
            PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Player2"), spawnPoint2.transform.position, Quaternion.identity);           
        }
    }  
}
