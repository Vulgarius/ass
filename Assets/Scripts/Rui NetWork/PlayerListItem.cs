﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerListItem : MonoBehaviourPunCallbacks
{
    //quando o player joined the room o script laucher vai receber uma callback e vai instanciar a Playerlistitem e se sair vai destruir
    // Start is called before the first frame update

    [SerializeField] TMP_Text text;
    Player player;
    public void SetUp(Player _player)  // criacao do player
    {
        player = _player;
        text.text = _player.NickName;
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        if(player == otherPlayer)
        {
            Destroy(gameObject);
        }
    }

    public override void OnLeftRoom()
    {
        Destroy(gameObject);
    }

    
}
