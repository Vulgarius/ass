﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using System.IO;

public class RoomManager : MonoBehaviourPunCallbacks
{ 
    public static RoomManager Instance;
    

    private void Awake()
    {
        if (Instance) // veririca se ja existe um RoomManager 
        {
            Destroy(gameObject); // se sim destroy
            return;
        }
        DontDestroyOnLoad(gameObject); // se nao este passa a ser o RoomManager capaz de comunicar entre os jogadores
        Instance = this; 
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
    {      
        if(scene.buildIndex == 1)
        {
            PhotonNetwork.Instantiate("PhotonPrefabs/PlayerManager", Vector3.zero, Quaternion.identity);

        }
    }

    public override void OnEnable()
    {
        base.OnEnable();
        SceneManager.sceneLoaded += OnSceneLoaded;      
    }

    public override void OnDisable()
    {
        base.OnDisable();
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
