﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;
using System.Linq;

public class Launcher : MonoBehaviourPunCallbacks
{

    public static Launcher Instace; //utiliza uma singleton de modo a chamar este metodo

    [SerializeField] TMP_InputField roomNameInputField;
    [SerializeField] TMP_Text errorText;
    [SerializeField] TMP_Text roomNameText;
    [SerializeField] Transform roomListContent;
    [SerializeField] Transform playerListContent;
    [SerializeField] GameObject roomListItemPrefab;
    [SerializeField] GameObject PlayerListItemPrefab;
    [SerializeField] GameObject startGameButtom; // referencia do buttom de modo so ao H poder iniciar o jogo

   

    private void Awake()
    {
        Instace = this;
    }
    void Start()
    {
        PhotonNetwork.GameVersion = "0.0.1";
        PhotonNetwork.ConnectUsingSettings(); // conecta ao sistema do photon usando as configuracoes mencionadas anteriormente
    }

    public override void OnConnectedToMaster() //Called when the client is connected to the Master Server and ready for matchmaking and other tasks
    {
        Debug.Log("Connect to Server Photon");
        PhotonNetwork.JoinLobby(); // modo de encontrar rooms
        PhotonNetwork.AutomaticallySyncScene = true; // qd o Host muda de scene o photon vai atualizar as scenas para todos os outros utilizadores
    }

    public override void OnJoinedLobby()
    {
        MenuManager.Instance.OpenMenu("Title");
        Debug.Log("Joined Lobby");
        PhotonNetwork.NickName = "Player " + Random.Range(0, 1000).ToString("0000");
    }

    public void CreateRoom()
    {
        if (string.IsNullOrEmpty(roomNameInputField.text))
        {
            return; // verifica se realmente entramos numa sala
        }
        PhotonNetwork.CreateRoom(roomNameInputField.text); // cria uma nova sala 
        MenuManager.Instance.OpenMenu("Loading..."); // permite q os jogadores nao selecionem em outros botoes quando entram
    }

    // quando criamos uma sala chama dois eventos

    public override void OnJoinedRoom() // apos ser criado a sala vai ser possivel juntar e aqui vai fazer a atualizacao da sala
    {
        MenuManager.Instance.OpenMenu("room");
        roomNameText.text = PhotonNetwork.CurrentRoom.Name; // vai dar o nome do room

        Player[] players = PhotonNetwork.PlayerList;

        foreach (Transform child in playerListContent)
        {
            Destroy(child.gameObject); // limpa todos os jogadores adicionados nas salas anteriores
        }

        for (int i = 0; i < players.Count(); i++) // verifica o numero total de players
        {
            if (PhotonNetwork.PlayerList.Length <= 2) // se o nuemero total for inferior a 2  mudeo .Length 
            {
                Instantiate(PlayerListItemPrefab, playerListContent).GetComponent<PlayerListItem>().SetUp(players[i]); //cria um novo jogador
                          
            }
            else if(PhotonNetwork.CurrentRoom.PlayerCount ==2)
            {
                PhotonNetwork.CurrentRoom.IsVisible = false; // se nao a sala passa a indisponivel
            }
        }

        startGameButtom.SetActive(PhotonNetwork.IsMasterClient); // se nao for o H a juntar o botao nao sera ativo


    }

    public override void OnMasterClientSwitched(Player newMasterClient) // se o H sair outro jogador torna-se H
    {
        startGameButtom.SetActive(PhotonNetwork.IsMasterClient); // se nao for o H a juntar o botao nao sera ativo
    }
    

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        errorText.text = "Room Creation Failed: " + message;
        MenuManager.Instance.OpenMenu("Error");
    }

    public void StartGame()
    {
        PhotonNetwork.LoadLevel(1);
    }

    public void JoinRoom(RoomInfo info) // modod de juntar ao room ja criado
    {
        PhotonNetwork.JoinRoom(info.Name);
        MenuManager.Instance.OpenMenu("Loading...");
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom(); // sai da sala
        MenuManager.Instance.OpenMenu("Loading...");
    }

    public override void OnLeftRoom()
    {
        MenuManager.Instance.OpenMenu("Title");  // quando sair do room abre o title menu
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList) // da a lista de numero de players
    {
        foreach (Transform trans in roomListContent) // atualiza as listas a cada updade 
        {
            Destroy(trans.gameObject); // destroi os botoes das listas anteriores e atualiza a nova
        }

        for (int i=0; i< roomList.Count; i++)  
        {
            if (roomList[i].RemovedFromList) //Photon nao remove da lista a sala mas coloca como falso caso saia,desta forma verifica q ja foi chamda e Nao instancia novamente
                continue;
            Instantiate(roomListItemPrefab, roomListContent).GetComponent<RoomListItem>().Setup(roomList[i]); //modo de se conectar a room a partir do botao de cliqye RoomListItem
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Instantiate(PlayerListItemPrefab, playerListContent).GetComponent<PlayerListItem>().SetUp(newPlayer);
        
    }
}
