﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    
    public string menuName;
    public bool open; // vai permiir verificar qual dos menus esta aberto

   public void Open()
    {
        open = true;
        gameObject.SetActive(true); // ativa o menu
    }

    public void Close()
    {
        open = false;
        gameObject.SetActive(false); // desativa o menu
    }

   
}
