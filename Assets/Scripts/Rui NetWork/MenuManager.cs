﻿//using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{

    // criar uma singleton de modo a obter em todo lado do jogo
    public static MenuManager Instance;

    [SerializeField] Menu[] menus;

    private void Awake()
    {
        Instance = this;
    }

    public void OpenMenu(string menuName) // permite abrir o menu ser ter que estar a referenciado
    {
        for(int i = 0; i< menus.Length; i++)
        {
            if(menus[i].menuName == menuName) // verifica se e o menu que pretendo abrir
            {
                menus[i].Open();
            }
            else if(menus[i].open)
            {
                CloseMenu(menus[i]);
            }
        }
    }

    public void OpenMenu(Menu menu)
    {
        for (int i = 0; i < menus.Length; i++)
        {
            if (menus[i].open) // verifica se e o menu que pretendo abrir
            {
                CloseMenu(menus[i]); // modo de ter um menu aberto apenas
            }
        }
            menu.Open(); // referencia no script Menu

    }

    public void CloseMenu(Menu menu)
    {
        menu.Close(); // referencia no script Menu

    }

    public void CloseGame()
    {
        Application.Quit();
    }


}
