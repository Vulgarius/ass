using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEngine.Events;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;

public class Fios : MonoBehaviour
{
    [SerializeField] private GameObject panelGameOver;
    [SerializeField] private GameObject panelVictory;
    public GameObject[] PainelPrefab = new GameObject[16];
    public Transform wiresSpawn, cameraReference;
    public CameraMoveGetter cameraMoveGetter;
    public UnityEvent activate;
    private bool enter;
    private bool solved;
    private Transform _cameraBackup;
    private int _counter=0;
    private bool dead;

    private float x, y, z;
    
    private int _cable;
    // Start is called before the first frame update
    void Start()
    {
        _cable = Random.Range(0, 16);
        PainelPrefab[_cable].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (enter)
        {
            Enter();   
        }
    }

    private void Enter()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cameraMoveGetter.mainCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out var hit))
            {
                if (hit.collider.tag=="Certo" && !dead)
                {
                    //Destroy(hit.collider.gameObject);
                    solved = true;
                    //ChangeCamera();
                    panelVictory.SetActive(true);
                }
                else if (hit.collider.tag=="Errado")
                {
                    dead = true;
                    panelGameOver.SetActive(true);
                }
            }
        }
    }

    public void ChangeCamera()
    {
        if (solved)
        {
            cameraMoveGetter.mainCamera.transform.position = new Vector3(x,y,z);
            Cursor.lockState = CursorLockMode.Locked;
            cameraMoveGetter.SwitchStuff();
            enter = false;
        }
        else
        {
            x = cameraMoveGetter.mainCamera.transform.position.x;
            y = cameraMoveGetter.mainCamera.transform.position.y;
            z = cameraMoveGetter.mainCamera.transform.position.z;
            enter = true;
            GetComponent<BoxCollider>().enabled = false;
            cameraMoveGetter.mainCamera.transform.position = cameraReference.position;
            cameraMoveGetter.mainCamera.transform.rotation = cameraReference.rotation;
            Cursor.lockState = CursorLockMode.None;
            cameraMoveGetter.SwitchStuff();
        }
    }
}
