﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitcher : MonoBehaviour
{

    public Material[] camerasMaterial;
    public GameObject[] cameras;
    private int activeCamera = 0;
    private MeshRenderer meshRenderer;
    
    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
    }
    
    public void NextCamera()
    {
        cameras[activeCamera].SetActive(false);
        if (activeCamera == 8)
        {
            activeCamera = 0;
        }
        else
        {
            activeCamera++;
        }
        cameras[activeCamera].SetActive(true);
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/mouse-click-sound-effect-hd");
    
        meshRenderer.sharedMaterial = camerasMaterial[activeCamera];
    }
}
