using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField] private GameObject cinematicCamera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            cinematicCamera.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            cinematicCamera.SetActive(false);
        }
    }
}
