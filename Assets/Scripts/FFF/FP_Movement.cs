﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class FP_Movement : MonoBehaviour
{
    
    public CharacterController controller;
    private FMOD.Studio.EventInstance passos;
    public float speed = 3f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;

    public GameObject feet;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    
    private Vector3 velocity;
    private bool isGrounded;
    
    PhotonView PV;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
        controller = GetComponent<CharacterController>();
    }

    private void Start()
    {
        //if (!PV.IsMine)
        //{
        //   Destroy(GetComponentInChildren<Camera>().gameObject);
        //Destroy(GetComponent<CharacterController>());
        // }

        passos = FMODUnity.RuntimeManager.CreateInstance("event:/SFX/FootSteps");
        passos.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(feet));

    }
    void Update()
    {
        //if (!PV.IsMine)
        //{
        //    return;
        //}
        

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
            }

            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            Vector3 move = transform.right * x + transform.forward * z;

            // if (z == 0)
            // {
            //     passos.start();
            //     speed = _baseSpeed;
                //passos.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                //passos.release();
            // }
            // else
            // {
            //
            //   
            // }
            //else if (z < 0)
            //{
            //passos.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
            //speed = _baseSpeed / 2.5f;
            //}
                

            controller.Move(move * (speed * Time.deltaTime));

            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                velocity.y = (float)Math.Sqrt(jumpHeight * -2f * gravity);
            }

            velocity.y += gravity * Time.deltaTime;

            controller.Move(velocity * Time.deltaTime);
        }

        
    
}
