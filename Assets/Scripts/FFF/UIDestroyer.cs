﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDestroyer : MonoBehaviour
{
   private GameObject ui;
       private bool destroyed;
    
   
       private void Start()
       {
           ui = GameObject.FindGameObjectWithTag("PlayerUI");
       }
    
       private void Update()
       {
           if(ui != null)
           {
               ui.SetActive(false);
           }
       }
}
