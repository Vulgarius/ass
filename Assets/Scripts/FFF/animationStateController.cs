﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Chat;
using Photon.Pun;


public class animationStateController : MonoBehaviour
{
    private Animator animator;
    private static readonly int IsWalking = Animator.StringToHash("isWalking");
    private static readonly int IsWalkingBackwards = Animator.StringToHash("isWalkingBackwards");
    private static readonly int IsStrafingLeft = Animator.StringToHash("isStrafingLeft");
    private static readonly int IsStrafingRight = Animator.StringToHash("isStrafingRight");

    //PhotonViewAnimator PV;

    PhotonAnimatorView PV;
    private void Awake()
    {
        animator = GetComponent<Animator>();
        PV = GetComponent<PhotonAnimatorView>();
    }

    void Start()
    {
        if (!PV.isActiveAndEnabled)
        {
            Destroy(animator);
        }
    }

    // Update is called once per frame
    void Update()
    {
        

            float verticalPress = Input.GetAxis("Vertical");
            float horizontalPress = Input.GetAxis("Horizontal");

            bool isWalking = animator.GetBool(IsWalking);
            bool isWalkingBackwards = animator.GetBool(IsWalkingBackwards);
            bool isStrafingLeft = animator.GetBool(IsStrafingLeft);
            bool isStrafingRight = animator.GetBool(IsStrafingRight);

            if (!isWalking && verticalPress > 0)
            {
                animator.SetBool(IsWalking, true);
            }
            else if (!isWalkingBackwards && verticalPress < 0)
            {
                animator.SetBool(IsWalkingBackwards, true);
            }
            else if (verticalPress == 0)
            {
                animator.SetBool(IsWalking, false);
                animator.SetBool(IsWalkingBackwards, false);
            }

            if (!isStrafingLeft && horizontalPress < 0)
            {
                animator.SetBool(IsStrafingLeft, true);
            }
            else if (!isStrafingRight && horizontalPress > 0)
            {
                animator.SetBool(IsStrafingRight, true);
            }
            else if (horizontalPress == 0)
            {
                animator.SetBool(IsStrafingLeft, false);
                animator.SetBool(IsStrafingRight, false);
            }

        
            
    }
        
    
}
