﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideDoor : MonoBehaviour
{

    private Animator AnimDoor;
    
    private bool _isActive;
    public bool _isUnlocked = false;
    private static readonly int IsActive = Animator.StringToHash("IsActive");
    public GameObject endCanvas;

    [FMODUnity.EventRef]
    public string Event;
    public bool PlayOnAwake;

    private void Start()
    {
        AnimDoor = GetComponent<Animator>();
    }
    
    public void Door()
    {
        if (_isUnlocked || gameObject.name == "bathroom door*")
        {
            if (_isActive)
            {
                AnimDoor.SetBool(IsActive, false);
                _isActive = false;
                FMODUnity.RuntimeManager.PlayOneShotAttached(Event, gameObject);
            }
            else
            if (!_isActive)
            {
                AnimDoor.SetBool(IsActive, true);
                _isActive = true;
                FMODUnity.RuntimeManager.PlayOneShotAttached(Event, gameObject);
            }
        }
    }
   
}
