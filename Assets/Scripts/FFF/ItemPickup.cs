﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ItemPickup : MonoBehaviour
{

    public UnityEvent UseButton;

    public GameObject itemPrefab;   
    private GameObject mainCamera;   
    private GameObject reference;


    private PlayerMove _playerMove;


    private float _timeStartedLerping;

    private Vector3 _startPosition;
    private Vector3 _endPosition;

    private bool _lerpIn;

    private FP_Camera _fpCamera;

    private Vector3 _cameraRotation;

    private Lerper _lerper;

    private GameObject _ui;

    private Timer countdown;

    
    void Start()
    {
        _lerper = GetComponent<Lerper>();
        _startPosition = itemPrefab.transform.position;
        countdown = FindObjectOfType<Timer>();
    }

    void Update()
    {
        if (_lerpIn)
        {
            itemPrefab.transform.position = _lerper.Lerp(_startPosition, _endPosition, _timeStartedLerping);
        }

        Inicializacao();
    }

    IEnumerator ItemToInventory()
    {
        _ui.SetActive(false);  // desabilita bola do mouse
        yield return new WaitForSeconds(5f); 
        _ui.SetActive(true);
        _fpCamera.enabled = true;
        _playerMove.enabled = true;
        Destroy(gameObject);
    }

    private void LerpIn()
    {
        _lerpIn = true;
        _timeStartedLerping = Time.time;

        _endPosition = reference.transform.position;
    }

    public void PickThisUp()  // orientacao da camera
    {
        LerpIn(); //calls lerping function
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/grab-foley-sound-fx");

        itemPrefab.transform.forward = -mainCamera.transform.right; //orients object facing camera

        Vector3 h = itemPrefab.transform.localRotation.eulerAngles; //gets rotation values for prefab
        _cameraRotation = mainCamera.transform.rotation.eulerAngles; //gets rotation values for camera

        itemPrefab.transform.localRotation = Quaternion.Euler(h.x, h.y, -_cameraRotation.x); //object adapts to camera rotation
                
        _fpCamera.enabled = false; //disable camera look
        _playerMove.enabled = false; 

        StartCoroutine(ItemToInventory());
    }

    void Inicializacao()
    {
        if(reference == null && mainCamera == null)
        {
            if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            {
                reference = GameObject.FindGameObjectWithTag("headSetReferencia");
                mainCamera = GameObject.FindGameObjectWithTag("camera");
                _playerMove = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMove>();
                _ui = GameObject.FindGameObjectWithTag("PlayerUI");

                _fpCamera = mainCamera.GetComponent<FP_Camera>();

               countdown.ClickPlay();
            }
        }
    }


   


}
