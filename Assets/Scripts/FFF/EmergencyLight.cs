﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmergencyLight : MonoBehaviour
{

    public Material OnLight, OffLight;

    private MeshRenderer _meshRenderer;
    private Material[] _materials;

    private bool _isLightBlinking = true;
    
    // Start is called before the first frame update
    void Start()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _materials = _meshRenderer.sharedMaterials;
        StartCoroutine(SwitchLights());
    }

    public void StopLights()
    {
        _isLightBlinking = false;
    }
    
    private IEnumerator SwitchLights()
    {
        while (_isLightBlinking)
        {
            yield return new WaitForSeconds(0.5f);
            if (_meshRenderer.sharedMaterials[1] == OffLight)
            {
                _materials[1] = OnLight; //it's not possible to assign only one material of a sharedMaterials array. so we change the material on the array copy.
                _meshRenderer.sharedMaterials = _materials; //and then it's necessary to reassign the entire array, like so.
            }
            else
            {
                _materials[1] = OffLight;
                _meshRenderer.sharedMaterials = _materials;
            }
        }
        if(!_isLightBlinking)
        {
            _materials[1] = OffLight;
            _meshRenderer.sharedMaterials = _materials;
        }
    }
}
