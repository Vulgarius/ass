﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Pickups : MonoBehaviour
{
    public LayerMask raycastCollidableLayers; //Set this in inspector, makes you able to say which layers should be collided with and which not.
    public float checkDistance; // distancia aos objectos
    private GameObject ManualCanvas;

    private Animator animator;

    private GameObject knob, mouse, f; // mudei para privado
    private static readonly int pressed = Animator.StringToHash("Pressed");

    FlipPage flipP;
    pass novaTentativa;
    private ManualBehaviour manualBehaviour;

    [FMODUnity.EventRef]
    public string Event;

    private void Awake()
    {
        knob = GameObject.FindGameObjectWithTag("KnobRef");
        mouse = GameObject.FindGameObjectWithTag("MouseRef");
        f = GameObject.FindGameObjectWithTag("FRef");
        animator = GameObject.FindGameObjectWithTag("Button").GetComponent<Animator>();
    }

    private void Start()
    {
        flipP = FindObjectOfType<FlipPage>();
        novaTentativa = FindObjectOfType<pass>();
        manualBehaviour = FindObjectOfType<ManualBehaviour>();
    }
    void Update()
    {
        {
            PerformRaycast();
        }
    }

    private void PerformRaycast()
    {
        if (Physics.Raycast(transform.position, transform.forward, out var hit, checkDistance, raycastCollidableLayers))
        {
            if (hit.collider.name == "Lights" || hit.collider.name == "Botao" || hit.collider.name == "Porta"
                || hit.collider.name == "PrevButton" || hit.collider.name == "NextButton" ||
                hit.collider.name == "Cofre(Clone)" || hit.collider.name == "Display" ||
                hit.collider.name == "Manual" || hit.collider.name == "WireManager" || hit.collider.name == "SafePuzzle") 
            {
                knob.SetActive(false);
                f.SetActive(false);
                mouse.SetActive(true);

                if (hit.collider.name == "Botao")
                {
                    mouse.GetComponent<Image>().color = Color.black;
                }

                if (Input.GetMouseButtonDown(0))
                {
                    ManualCanvas = GameObject.FindGameObjectWithTag("Manual");
                    if (hit.collider.name == "Botao")
                    {
                        animator.SetTrigger(pressed);
                        ButtonBehaviour button = hit.collider.GetComponent<ButtonBehaviour>();
                        button.UseButton.Invoke();
                    }
                    else if (hit.collider.name == "Porta")
                    {
                        ButtonBehaviour button = hit.collider.GetComponent<ButtonBehaviour>();
                        button.UseButton.Invoke();
                    }
                    else if (hit.collider.name == "Lights")
                    {
                        Keypad keypad = hit.collider.GetComponent<Keypad>();
                        keypad.UseButton.Invoke();
                    }
                    
                    else if (hit.collider.name == "NextButton")
                    {
                        flipP.TurnNextPage();
                    }
                    else if (hit.collider.name == "PrevButton")
                    {
                        flipP.TurnPreviousPage();
                    }
                    else if (hit.collider.name == "Cofre(Clone)")
                    {
                        novaTentativa.activateCanvas();
                    }
                    else if (hit.collider.name == "Display")
                    {
                        CameraSwitcher cameraSwitcher = hit.collider.GetComponent<CameraSwitcher>();
                        cameraSwitcher.NextCamera();
                    }
                    else if (hit.collider.name == "Manual")
                    {
                        manualBehaviour.UIToggle();
                    }
                    else if (hit.collider.name == "WireManager")
                    {
                        Fios fios = hit.collider.GetComponent<Fios>();
                        fios.activate.Invoke();
                    }
                    else if (hit.collider.name == "SafePuzzle")
                    {
                        hit.collider.gameObject.GetComponent<Animator>().SetTrigger("Open");
                        FMODUnity.RuntimeManager.PlayOneShotAttached(Event, gameObject);
                    }
                }
            }
            else if (hit.collider.name == ("fonederadio") || hit.collider.name == "cartao")
            {
                knob.SetActive(false);
                f.SetActive(true);
                mouse.SetActive(false);

                if (Input.GetKeyDown(KeyCode.F) && hit.collider.GetComponent<ItemPickup>())
                {
                    ItemPickup pickup = hit.collider.GetComponent<ItemPickup>();
                    pickup.UseButton.Invoke();
                }
            }
        }
        else
        {
            mouse.GetComponent<Image>().color = Color.white;
            knob.SetActive(true);
            mouse.SetActive(false);
            f.SetActive(false);
        }
    }
}