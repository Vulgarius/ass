﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Keypad : MonoBehaviour
{
    public CameraMoveGetter cameraMoveGetter;
    
    public GameObject alarme3D;
    public GameObject radio3D;
    public UnityEvent UseButton;

    public SlideDoor flightDeckDoor;
    
    public Material onGreen;
    public Material onYellow;
    public Material onRed;

    private FMOD.Studio.EventInstance alarme;
    private FMOD.Studio.EventInstance radio;
    public PlayerMove FpMovement;         

    public GameObject button;
    private MeshRenderer _buttonMeshRenderer;

    public GameObject KeypadPrefab; 
    private GameObject MainCamera;    //mudei para privado
    private GameObject Reference;    //mudei para privado
   

    private MeshRenderer[] _meshRenderers;
    private Material[] _offColors;
    private Material _currentMaterial;
    
    private Camera _camera;
    private FP_Camera _fpCamera;
    private Transform _cameraTransform;

    private FP_Movement _fpMovement;
    
    private Lerper _lerper;
    private bool _lerpIn = false;
    private bool _lerpOut = false;

    private Vector3 startPosition;
    private Vector3 endPosition;

    private float timeStartedLerping;

    private Vector3 _keypadForward;
    private Vector3 _keypadOriginalRotation;
    private Vector3 _cameraRotation;

    private BoxCollider _boxCollider;

    public bool _puzzable = false;

    private CheatInput _cheatInput;
    
    public GameObject ui;
    
    public PausaPC pausaPC;
    
    // Start is called before the first frame update
    private void Start()
    {
        _meshRenderers = GetComponentsInChildren<MeshRenderer>();
        _lerper = GetComponent<Lerper>();
        _boxCollider = GetComponent<BoxCollider>();
        _buttonMeshRenderer = button.GetComponent<MeshRenderer>();
        _cheatInput = GetComponent<CheatInput>();
        _cheatInput.enabled = false;

        alarme = FMODUnity.RuntimeManager.CreateInstance("event:/BKG/Emergency");
        alarme.start();
        alarme.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(alarme3D));
        
        _offColors = new Material[_meshRenderers.Length];

        for (int i = 0; i < 3; i++)
        {
            _offColors[i] = _meshRenderers[i].sharedMaterial;
        }
    }

    void Update()
    {
        if (_lerpIn)
        {
            KeypadPrefab.transform.position = _lerper.Lerp(startPosition, endPosition, timeStartedLerping);
        }
        if (_lerpOut)
        {
            KeypadPrefab.transform.position = _lerper.Lerp(endPosition, startPosition, timeStartedLerping);
        }
        if (_puzzable)
        {
            BeginPuzzle();
        }
        if (Input.GetKeyUp(KeyCode.Escape) && _puzzable)
        {
            DeactivateInteraction();
        }

        Inicializacao();
    }

    public void ActivateInteraction()
    {
        LerpIn(); //calls lerping function
        
        KeypadPrefab.transform.forward = -MainCamera.transform.right; //orients object facing camera
        
        Vector3 k = KeypadPrefab.transform.rotation.eulerAngles; //gets rotation values for keypad
        _cameraRotation = MainCamera.transform.rotation.eulerAngles; //gets rotation values for camera
        
        KeypadPrefab.transform.rotation = Quaternion.Euler(k.x,k.y,-_cameraRotation.x); //object adapts to camera rotation
        
        Cursor.lockState = CursorLockMode.None; //enables mouse pointer
        cameraMoveGetter.SwitchStuff();
        //_fpCamera.enabled = false; //disable camera look
        //FpMovement.enabled = false;
        ui.SetActive(false);
        
        _boxCollider.enabled = false;
        //_isActivated = true;
        pausaPC.pausable = false;
        _puzzable = true;
        _cheatInput.enabled = true;
    }

    IEnumerator Waiter()
    {
        yield return new WaitForSeconds(1f);
    }
    
    public void DeactivateInteraction()
    {
        LerpOut();

        KeypadPrefab.transform.forward = _keypadForward; //orients object facing its original position

        KeypadPrefab.transform.rotation = Quaternion.Euler(_keypadOriginalRotation.x,_keypadOriginalRotation.y,_keypadOriginalRotation.x); // back to object's original rotation
        
        Cursor.lockState = CursorLockMode.Locked;
        cameraMoveGetter.SwitchStuff();
        //_fpCamera.enabled = true;
        //FpMovement.enabled = true;
        ui.SetActive(true);

        _boxCollider.enabled = true;
        //_isActivated = false;
        pausaPC.pausable = true;
        _puzzable = false;
        _cheatInput.enabled = false;
    }

    private void LerpIn()
    {
        _lerpIn = true;
        timeStartedLerping = Time.time;
        
        endPosition = Reference.transform.position;
        _lerpOut = false;
    }

    private void LerpOut()
    {
        _lerpOut = true;
        timeStartedLerping = Time.time;
        
        endPosition = Reference.transform.position;
        _lerpIn = false;
    }

    IEnumerator EndPuzzle()
    {
        
        yield return new WaitForSeconds(1f);
        DeactivateInteraction();
        _boxCollider.enabled = false;
        flightDeckDoor._isUnlocked = true;
        yield return new WaitForSeconds(1f);        

    }
    
    IEnumerator ResetPuzzle()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Fail_Code");
        
        yield return new WaitForSeconds(1f);
        _meshRenderers[0].sharedMaterial = _offColors[0];
        yield return new WaitForSeconds(0.3f);
        _meshRenderers[1].sharedMaterial = _offColors[1];
        yield return new WaitForSeconds(0.3f);
        _meshRenderers[2].sharedMaterial = _offColors[2];
    }

    public void PuzzleSuccess()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Correct_Song");
        alarme.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        alarme.release();
        _meshRenderers[0].sharedMaterial = onGreen;
        _meshRenderers[1].sharedMaterial = onGreen;
        _meshRenderers[2].sharedMaterial = onGreen;
        _buttonMeshRenderer.material = onGreen;
        StartCoroutine(EndPuzzle());
    }
    
    public void PuzzleFail()
    {
        _meshRenderers[0].sharedMaterial = onRed;
        _meshRenderers[1].sharedMaterial = onRed;
        _meshRenderers[2].sharedMaterial = onRed;
        StartCoroutine(ResetPuzzle());
    }
    
    public void BeginPuzzle()
    {
        _cheatInput.ReadInput();
    }

    void Inicializacao()
    {
        if (Reference == null && MainCamera == null)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                Reference = GameObject.FindWithTag("keypadReferencia");
                MainCamera = GameObject.FindWithTag("camera");

                _fpCamera = MainCamera.GetComponent<FP_Camera>();

                startPosition = KeypadPrefab.transform.position;
                _keypadForward = KeypadPrefab.transform.forward;
                _keypadOriginalRotation = KeypadPrefab.transform.rotation.eulerAngles; //gets rotation values for keypad
            }
        }



    }
}
