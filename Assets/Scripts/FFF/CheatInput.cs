﻿using System;
using UnityEngine;
using UnityEngine.Events;
 
public class CheatInput : MonoBehaviour
{
    public KeyCode[] codeKeyboard;
    public string[] codeMouse;
    public UnityEvent inputSuccess;
    public UnityEvent inputFail;
    public float allowedDelay = 1f;

    private Animator _keysAnimator;
 
    //private float _delayTimer;
    private int _index = 0;
    private int _erros = 0;
    
    public LayerMask raycastCollidableLayers; //Set this in inspector, makes you able to say which layers should be collided with and which not.
    public float checkDistance = 1f;
    RaycastHit _hit;
    
    
    private string _keyCode;

    private void Start()
    {
        _keysAnimator = GetComponentInParent<Animator>();
    }

    public void ReadInput()
    {
        //_delayTimer += Time.deltaTime;
        // if (_delayTimer > AllowedDelay)
        // {
        //     ResetCheatInput();
        //     InputFail.Invoke();
        // }
        
        // if (Input.anyKeyDown)
        // {
            if (Input.GetMouseButtonDown(0))
            {
                Ray ray = GameObject.FindGameObjectWithTag("camera").GetComponent<Camera>().ScreenPointToRay (Input.mousePosition);
                if (Physics.Raycast(ray, out _hit, 1, raycastCollidableLayers))
                {
                    FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Botão_Keypad");
                
                    if (_hit.collider.name == codeMouse[_index]) // se for imput certo
                    {
                            _keysAnimator.SetTrigger(_hit.collider.name);
                            _index++;                           //passa para o proximo
                    }
                    else  //se for errado
                    {
                        _keysAnimator.SetTrigger(_hit.collider.name);
                    _index++;
                    _erros++; //aumenta os erros
                    }  
                }
            }
        //     else if (Input.GetKeyDown(codeKeyboard[_index]))
        //     {
        //         _keyCode = Input.anyKeyDown.ToString();
        //         _keysAnimator.SetTrigger(_keyCode);
        //         _index++;
        //         //_delayTimer = 0f;
        //     }
        //     else
        //     {
        //         _keyCode = Input.anyKeyDown.ToString();
        //         _keysAnimator.SetTrigger(_keyCode);
        //         ResetCheatInput();
        //         inputFail.Invoke();
        //     }
        // }
        
        if (_index == codeMouse.Length ) // quando chega ao fim e carregar no botao verde
        {
            if(_erros == 0) // nao tiver erros
            {
                ResetCheatInput();
                inputSuccess.Invoke();   //sucesso
            }
            else //se tiver erros
            {
                _keysAnimator.SetTrigger(_hit.collider.name); 
                ResetCheatInput();
                inputFail.Invoke();
            }
            
        }

       
    }
 
    void ResetCheatInput()
    {
        _index = 0;
        _erros = 0;
        //_delayTimer = 0f;
    }
}