﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lerper : MonoBehaviour
{
    private const float LerpTime = 0.5f;  // tempo do lerp
    
    public Vector3 Lerp(Vector3 start, Vector3 end, float timeStartedLerping)
    {
        float timeSinceStarted = Time.time - timeStartedLerping; // qd o lerp

        float percentageComplete = timeSinceStarted / LerpTime; //velocidade linear a cada instante

        var result = Vector3.Lerp(start, end, percentageComplete); // executa o lerp

        return result;
    }
}
