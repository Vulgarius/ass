using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoveGetter : MonoBehaviour
{
    //Camera
    public GameObject mainCamera;
    public FP_Camera fpCamera;
    //Move
    private GameObject player;
    private PlayerMove playerMove;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GetStuff());
    }

    private IEnumerator GetStuff()
    {
        while (!fpCamera && !playerMove)
        {
            yield return new WaitForSeconds(0.02f);
            mainCamera = GameObject.Find("Main Camera");
            player = GameObject.Find("Player(Clone)");
            if (mainCamera && player)
            {
                fpCamera = mainCamera.GetComponent<FP_Camera>();
                playerMove = player.GetComponent<PlayerMove>();
            }
        }
    }

    public void SwitchStuff()
    {
        if (fpCamera.enabled)
        {
            fpCamera.enabled = false;
            playerMove.enabled = false;
        }
        else
        {
            fpCamera.enabled = true;
            playerMove.enabled = true;
        }
    }
}
