﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManualBehaviour : MonoBehaviour
{

    public GameObject manualCanvas;

    public GameObject leftButton, rightButton;

    public GameObject[] pages = new GameObject[5];

    private bool uiActive = false;
    private GameObject mainCamera;
    private FP_Camera fpCamera;
    private BoxCollider boxCollider;
    private int _index = 0;
    
    private void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
        StartCoroutine(InitializeCamera());
    }

    private IEnumerator InitializeCamera()
    {
        while (!fpCamera)
        {
            yield return new WaitForSeconds(0.02f);
            mainCamera = GameObject.Find("Main Camera");
            if (mainCamera)
            {
                fpCamera = mainCamera.GetComponent<FP_Camera>();
            }
        }
    }
    
    public void UIToggle()
    {
        if (uiActive == false)
        {
            manualCanvas.SetActive(true);
            fpCamera.enabled = false;
            boxCollider.enabled = false;
            Cursor.lockState = CursorLockMode.None;
            uiActive = true;
        }
        else
        {
            manualCanvas.SetActive(false);
            fpCamera.enabled = true;
            boxCollider.enabled = true;
            Cursor.lockState = CursorLockMode.Locked;
            uiActive = false;
        }
    }

    private void UpdatePage(int delta)
    {
        if (_index == 0)
        {
            leftButton.SetActive(false);
            
        }
        else if (_index == 4)
        {
            rightButton.SetActive(false);
        }
        else
        {
            leftButton.SetActive(true);
            rightButton.SetActive(true);
        }
        
        pages[_index].SetActive(true);
        pages[_index+delta].SetActive(false);
        
    }
    
    public void Right()
    {
        _index++;
        UpdatePage(-1);
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/rigpageFlip");
    }
    
    public void Left()
    {
        _index--;
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/y2mate.com - Page Flip Sound Effects HD");
        UpdatePage(+1);
    }

    public void Exit()
    {
        UIToggle();
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/open-book-sound-effect");
    }
    
}
