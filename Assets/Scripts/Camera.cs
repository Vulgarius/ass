﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraP2 : MonoBehaviour
{
    public float speedH = 2.0f;
    public float speedV = 2.0f;

    private float yaw = 0.0f;
    private float pitch = 0.0f;

    private Animator _camera;

    


    public void left()
    {
                
        if (transform.rotation.y == 0)
        {
            //transform.rotation = Quaternion.Euler(0.0f, -90.0f, 0.0f);
            _camera.Play("1");

        }
         else if (transform.rotation.y > 0.0f)
        {
            //transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            _camera.Play("2");

        }
       
    }
    public void right()
    {
       
        if (transform.rotation.y == 0)
        {
            //transform.rotation = Quaternion.Euler(0.0f, 90.0f, 0.0f);
            _camera.Play("3");

        }
        else if (transform.rotation.y < 0.0f)
        {
            //transform.rotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
            _camera.Play("2");
        }


    }
    public void zoom1()
    {
        _camera.Play("1_Zoom");
    }

    public void zoom2()
    {
        _camera.Play("2_Zoom");
    }

    public void zoom3()
    {
        _camera.Play("3_Zoom");
    }
    // Start is called before the first frame update
    void Start()
    {
        _camera = GetComponent<Animator>();

        
        
    }

    // Update is called once per frame
    void Update()
    {
        //yaw += speedH * Input.GetAxis("Mouse X");
        //pitch -= speedV * Input.GetAxis("Mouse Y");

        //transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
        
    }
}
