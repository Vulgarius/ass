using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeControl : MonoBehaviour
{
    FMOD.Studio.Bus master;

    float MasterVolume = 1;

    private void Awake()
    {
        master = FMODUnity.RuntimeManager.GetBus("bus:/Master");
    }

    private void Update()
    {
        master.setVolume(MasterVolume);
    }

    public void MasterVolumeLevel(float newVolumeMaster)
    {
        MasterVolume = newVolumeMaster;
    }
}
