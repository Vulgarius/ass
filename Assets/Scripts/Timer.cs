﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField] private GameObject panelGameOver;
    public float timeRemaining = 15000;
    private bool timePlaying = false;
    public Text timeText;
    private int final = 0;

    private void Start()
    {
        timePlaying = false;
        timeText.color = Color.green;
    }

    void FixedUpdate()
    {
        if (timePlaying)
        {
            if (timeRemaining > 10000 && timeRemaining <= 15000)
            {
                timeRemaining -= Time.deltaTime * 45f;
                timeText.color = Color.green;
                timeText.text = string.Format("{0}", timeRemaining);
            }
            if (timeRemaining > 5000 && timeRemaining <= 10000)
            {
                timeRemaining -= Time.deltaTime * 90f;
                timeText.color = Color.yellow;
                timeText.text = string.Format("{0}",timeRemaining);
            }
            if (timeRemaining <= 5000 && timeRemaining >0)
            {
                timeRemaining -= Time.deltaTime * 373f;
                timeText.color = Color.red;
                timeText.text = string.Format("{0}", timeRemaining);
            }
            if (timeRemaining == 0 || timeRemaining <0 ) // verificar condicao se entrou na porta
            {
                timePlaying = false;
                timeText.color = Color.red;
                timeText.text = string.Format("{0}", final);
                panelGameOver.SetActive(true);
                Cursor.lockState = CursorLockMode.None;
                // se nao entrou na porta e o tempo terminou colocar o codigo de perda
            }
        }
    }

    public void ClickPlay()  // verifico se inicio o jogo carregando no W no script itemPickup
    {
        timePlaying = true;
    }

    //public void ClickStop()  // verifico se entro na porta de forma a para o tempo
    //{
    //    playing = false;
    //    carrego a cena de Win

    //}
}

