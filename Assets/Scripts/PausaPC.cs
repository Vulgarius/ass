﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausaPC : MonoBehaviour
{
    public CameraMoveGetter cameraMoveGetter;
    
    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;
    public GameObject manualUI;
    public GameObject cofreUI;
    
    public bool pausable = true;

    // Update is called once per frame
    void Update()
    {
        if (pausable)
        {
            if (manualUI.activeSelf == false)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    if (GameIsPaused)
                    {
                        Resume();
                    }else
                    {
                        Pause();
                    }
                }
            }
        }
    }
    void Resume()
    {
        pauseMenuUI.SetActive(false);
        cameraMoveGetter.SwitchStuff();
        //fpCamera.enabled = true;
        Cursor.lockState = CursorLockMode.Locked;
        GameIsPaused = false;
    }
    void Pause()
    {
        pauseMenuUI.SetActive(true);
        cameraMoveGetter.SwitchStuff();
        //fpCamera.enabled = false;
        Cursor.lockState = CursorLockMode.None;
        GameIsPaused = true;
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Menu()
    {
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel("Menu");
        //SceneManager.LoadSceneAsync("Menu");
        //SceneManager.LoadScene("Menu");
    }
    
}
