﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class pass : MonoBehaviour
{
    public CameraMoveGetter cameraMoveGetter;

    private string[] passWordStrings = {"CAMA","COCA","FEDE","FORD","GATO","KIRO","LOLE","LUAR","MEME","NILO","RICH","SOCA","TOPO","XPTO","YOLO"};
        public InputField inputUser;

        int chosenPass;

        private GameObject screen;
        private Material screenMaterial;
        public Texture[] morseTextures = new Texture[15];
        
        private GameObject cofre;
        public GameObject Cubo;
        private bool lockState;
        private Animator cofreAnim;
    
        bool codigoCerto;

        public PausaPC pausaPC;
        
        private void Start()
        {
            chosenPass = Random.Range(0, passWordStrings.Length);
            screen = GameObject.FindGameObjectWithTag("Screen");
            screen.GetComponent<Renderer>().sharedMaterial.SetTexture("_MainTex", morseTextures[chosenPass]);
            
            cofre = GameObject.Find("Cofre(Clone)");
            cofreAnim = cofre.GetComponent<Animator>();
        }
        
        public void TryPassword()
        {
            if (inputUser.text.ToLower() == passWordStrings[chosenPass].ToLower())
            {
              
                 FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Correct_Song");
                 FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/lockerOpen");
                 cofreAnim.Play("Open");
            

                codigoCerto = true;
                lockState = false;
                cameraMoveGetter.SwitchStuff();
                Debug.Log("entrei na 48");
                //fpCamera.enabled = true;
                pausaPC.pausable = true;
                Cubo.SetActive(false); 
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                cofre.GetComponent<BoxCollider>().enabled = true;
                lockState = false;
                FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Fail_Code");
                cameraMoveGetter.SwitchStuff();
                Debug.Log("entrei na 57");
                //fpCamera.enabled = true;
                pausaPC.pausable = true;
                Cubo.SetActive(false);
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    
        private void Update()
        {
            if (Cubo.gameObject.activeSelf)
            {
                switch (lockState)
                {
                    case true:
                        Cursor.lockState = CursorLockMode.None;
                        break;
                    case false:
                        Cursor.lockState = CursorLockMode.Locked;
                        break;
                }
            }
        }
    
        public void activateCanvas()
        {
            pausaPC.pausable = false;
            cofre.GetComponent<BoxCollider>().enabled = false;
            cameraMoveGetter.SwitchStuff();
            Cursor.lockState = CursorLockMode.None;
            Cubo.SetActive(true);
    
            EventSystem.current.SetSelectedGameObject(inputUser.gameObject, null);
            inputUser.OnPointerClick(new PointerEventData(EventSystem.current));
    
            lockState = true;
        }
}
