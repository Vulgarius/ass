﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCartoes : MonoBehaviour
{
    public Transform[] spawnerPoints; //locais onde os cartoes vao ser spawnados
    public Transform[] spawnQuadros;
    public GameObject[] quadros;
    public GameObject[] cofres;

    // quadro x montana  1 
    // quadro y cowbow   2
    // quadro z kiro     0
    // quadro k lobo     3

    public GameObject cofre;
    int randomInt;
    int randomQuadros;
   

    void Start()
    {
        if(cofre != null)
        {
            randomInt = Random.Range(0, spawnerPoints.Length);
            Instantiate(cofre, spawnerPoints[randomInt].position, spawnerPoints[randomInt].transform.rotation);
        }

        //criar 4 listas e ir associar o random as listas
        
        if(randomInt == 0) //localizacao 1
        {
            Instantiate(quadros[0], spawnQuadros[0].position, quadros[0].transform.rotation);
            Instantiate(quadros[1], spawnQuadros[1].position, quadros[1].transform.rotation);
            Instantiate(quadros[2], spawnQuadros[2].position, quadros[2].transform.rotation);
            Instantiate(quadros[3], spawnQuadros[3].position, quadros[3].transform.rotation);

        }
        else if( randomInt == 1) //localizacao 2
        {
            Instantiate(quadros[0], spawnQuadros[0].position, quadros[0].transform.rotation);
            Instantiate(quadros[1], spawnQuadros[1].position, quadros[1].transform.rotation);
            Instantiate(quadros[3], spawnQuadros[2].position, quadros[2].transform.rotation);
            Instantiate(quadros[2], spawnQuadros[3].position, quadros[3].transform.rotation);
        }
        else if(randomInt == 2) //localizacao 3
        {
            Instantiate(quadros[2], spawnQuadros[0].position, quadros[0].transform.rotation);
            Instantiate(quadros[1], spawnQuadros[1].position, quadros[1].transform.rotation);
            Instantiate(quadros[3], spawnQuadros[2].position, quadros[2].transform.rotation);
            Instantiate(quadros[0], spawnQuadros[3].position, quadros[3].transform.rotation);
        }

        else if(randomInt == 3) //localizacao 4
        {
            Instantiate(quadros[1], spawnQuadros[0].position, quadros[0].transform.rotation);
            Instantiate(quadros[0], spawnQuadros[1].position, quadros[1].transform.rotation);
            Instantiate(quadros[2], spawnQuadros[2].position, quadros[2].transform.rotation);
            Instantiate(quadros[3], spawnQuadros[3].position, quadros[3].transform.rotation);
        }

        else if (randomInt == 4) //localizacao 5
        {
            Instantiate(quadros[1], spawnQuadros[0].position, quadros[0].transform.rotation);
            Instantiate(quadros[3], spawnQuadros[1].position, quadros[1].transform.rotation);
            Instantiate(quadros[2], spawnQuadros[2].position, quadros[2].transform.rotation);
            Instantiate(quadros[0], spawnQuadros[3].position, quadros[3].transform.rotation);
        }
    }
}
